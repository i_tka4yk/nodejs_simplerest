var express = require('express');
var router = express.Router();
var uuidv4 = require('uuid/v4');

const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const path = require('path');

const adapter = new FileSync(path.join(__dirname ,'../model/db.json'));
const db = low(adapter);

router.get('/', (req, res) => {
  return res.send(JSON.stringify(db.get('users')));
});

router.get('/:userId', (req, res) => {
  let userId = req.params.userId;

  let user = db.get('users')
    .find({ _id: userId })
    .value();

  if(!user){
    return res.sendStatus(404);
  }

  return res.send(JSON.stringify(user));
});

router.post('/', (req, res) => {
  const user ={
    _id: uuidv4(),
    ...req.body
  };

  db.get('users')
  .push(user)
  .write()
  
  return res.send(JSON.stringify(user));
});

router.put('/:userId', (req, res) => {
  const userId = req.params.userId;

  let user = db.get('users')
    .find({ _id: userId })
    .value();

  if(!user){
    return res.sendStatus(404);
  }

  user = {
    ...user,
    ...req.body
  }

  db.get('users')
  .find({ _id: userId })
  .assign(user)
  .write()

  return res.send(JSON.stringify(user));
});

router.delete('/:userId', (req, res) => {
  const userId = req.params.userId;

  let user = db.get('users')
    .find({ _id: userId })
    .value();

  if(!user){
    return res.sendStatus(404);
  }

  db.get('users')
  .remove({ _id: userId })
  .write()

  return res.send(JSON.stringify(db.get('users')));
});

module.exports = router;
